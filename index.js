var express = require("express")
var app = express()
var channels = {HBO:["Silicon Valley","Game of Throws"], CW:["Supernatural","Arrow"], bravo:["CSI","NCIS"], Disney:["digimon","that so raven"], Toonomi:["Gundam 00","SAO II"]}

app.get("/channels",function(request, response){
    
    response.send(channels)
})
app.get("/channels/:show?",function(request, response) {
    var shows = request.params.show
    response.send(channels[shows])
})
app.listen(process.env.PORT)